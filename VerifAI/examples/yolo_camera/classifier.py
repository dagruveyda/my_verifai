import numpy as np
from dotmap import DotMap
import sys
sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
from verifai.client import Client

try:
    import tensorflow as tf
except ModuleNotFoundError:
    import sys
    sys.exit('This functionality requires tensorflow to be installed')

import sys
sys.path.append(r'E:\experiment\Automold--Road-Augmentation-Library')
import Automold as am
sys.path.append(r'E:\experiment\yolo-camera\yolov5-master\segment')
from predict_single import main
from test1 import getIoU
import cv2
import os
import pickle
import socket

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

class Classifier(Client):
	def __init__(self, classifier_data):
		port = classifier_data.port
		bufsize = classifier_data.bufsize
		super().__init__(port, bufsize)
		self.sess = tf.compat.v1.Session()
		# self.nn = Model()
		# self.nn.init(classifier_data.graph_path, classifier_data.checkpoint_path, self.sess)
		#self.lib = getLib()
		self.pos_array = np.array([[91, 54, 60],
							       [94, 55, 45],
								   [92, 39, 45], 
								   [90, 27, 45], 
								   [98, 25, 13],
								   [97, 52, 60],
								   [94, 55, 30]])

	def simulate(self, sample):
		# Create a socket
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect to the server
		server_address = ('localhost', 7000)
		sock.connect(server_address)
		# Send the car position to the server
		# numbers = [sample[6][0][0], sample[6][0][1], sample[6][0][2]]
		pose = self.pos_array[sample[3]]

		# sock.sendall(bytes(numbers))
		sock.sendall(bytes(list(pose)))

		# Receive the data
		data = b""
		while True:
			packet = sock.recv(1024)
			if not packet: break
			data += packet
		im = pickle.loads(data)
		im = im[:,:,0:3]

		# first apply object detection to the original image
		box, gr_classes, im_ground = main(im, show=False)#

		# check if yolo detects any objcets in the original image
		if box is not None:
			# ground = np.array([[int(box[0]), int(box[1])], [int(box[0]), int(box[3])], [int(box[2]), int(box[3])], [int(box[2]), int(box[1])]])
		
			## apply augmentations
			rain_type = sample[2]
			if rain_type != 'none':
				im = am.add_rain(im, rain_type=rain_type)
			
			# add fog
			fogg = sample[1]
			if fogg > 0: im = am.add_fog(im, fogg)
						
			# brightness value
			brightness = sample[0][0]
			if brightness > 0.0:
				im = am.brighten(im, brightness)
			else: im = am.darken(im, -brightness)

			# car class:2
			distorted, obj_classes, img_out = main(im, show=False)#
			if distorted is None:
				distorted = [np.array([[0, 0], [0, 0], [0, 0], [0, 0]])]
			#else:distorted = np.array([[int(distorted[0]), int(distorted[1])], [int(distorted[0]), int(distorted[3])], [int(distorted[2]), int(distorted[3])], [int(distorted[2]), int(distorted[1])]])
			IoU = getIoU(box, distorted, img_out.shape)
			# IoU = getIoU([ground], [distorted])
			print("intersection over uninon", IoU)
		else: 
			print("YOLO could not detect any objects, skipping augmentation.............")
			IoU = 0.0
			obj_classes = 80
			img_out = im
			#detected = False
		res = []
		# res.append(np.abs(0.7 - IoU))
		res.append(IoU)			
		if obj_classes is None:
			obj_classes = 80
		res.append(obj_classes)
		# output of YOLO
		res.append(img_out)
		# save also the ground truth image
		res.append(im_ground)
		# check if all the predicted classes are correct
		res.append(gr_classes != obj_classes)
		#res.append(detected)
		#return res
		return res



PORT = 8888
BUFSIZE = 4096

classifier_data = DotMap()
classifier_data.port = PORT
classifier_data.bufsize = BUFSIZE
classifier_data.graph_path = './data/car_detector/checkpoint/car-detector-model.meta'
classifier_data.checkpoint_path = './data/car_detector/checkpoint/'

client_task = Classifier(classifier_data)
while True:
	if not client_task.run_client():
		print("End of all classifier calls")
		break
