import numpy as np
from sklearn.decomposition import PCA
import pandas as pd

df = pd.read_csv(r'E:\experiment\my_verifai\VerifAI\examples\yolo_camera\tables\randomSampler\errorTable.csv', sep=',', header=None)
#print(df.values)
arr = df.values
arr = np.delete(arr, 0, 0)
arr = np.delete(arr, 0, 1)
arr = np.delete(arr, 17, 1)
arr = np.delete(arr, 18, 1)
arr = np.asarray(arr, dtype=float)

#X = np.array([[-1, -1, 3], [-2, -1, 5], [-3, -2, -2], [1, 1, 6], [2, 1, 3], [3, 2, -3]])
pca = PCA(n_components=3)
pca.fit(arr)
#print(pca.singular_values_)

# higher the weights in the 1st principal component, higher the feature's influence
print(abs( pca.components_ ))
