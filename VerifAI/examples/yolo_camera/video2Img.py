import cv2
import os
  
# Read the video from specified path
cam = cv2.VideoCapture("C:\\Users\\yilmaz\\Desktop\\15150001.MOV")
  
# try:
      
#     # creating a folder named data
#     if not os.path.exists('images'):
#         os.makedirs('images')
  
# # if not created then raise error
# except OSError:
#     print ('Error: Creating directory of data')
  
# frame
currentframe = 0
# ret_list = []  
while True:
      
    # take every 10th frame because the fps is high
    for i in range(10):
        ret,frame = cam.read()
    # ret_list.append(ret)
    if ret:
        # if video is still left continue creating images
        name = r'C:\Users\yilmaz\Desktop\real_imgs\frame' + str(currentframe) + '.jpg'
        print ('Creating...' + name)
  
        # writing the extracted images
        cv2.imwrite(name, frame)
  
        # increasing counter so that it will
        # show how many frames are created
        currentframe += 1
    else:
        break
  
# Release all space and windows once done
cam.release()
cv2.destroyAllWindows()
# print(ret_list)