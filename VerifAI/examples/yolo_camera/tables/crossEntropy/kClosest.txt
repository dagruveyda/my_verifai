SpacePoint(brightness=(-0.2412678880736268,), fogg=0.0, rain='heavy', shadow=(StructPoint(dimensions=5, exclude=1, no_shadows=2),), snow=(0.2090604113675513,), sun_flare=(StructPoint(exclude=1, no_secondary_flares=10, primary_radius=200, xPos=600, yPos=400),), z_positions=2)
#####################################################
SpacePoint(brightness=(-0.42120932952923285,), fogg=0.1, rain='none', shadow=(StructPoint(dimensions=7, exclude=1, no_shadows=1),), snow=(0.24632808852932325,), sun_flare=(StructPoint(exclude=0, no_secondary_flares=5, primary_radius=300, xPos=400, yPos=200),), z_positions=5)
#####################################################
SpacePoint(brightness=(0.4579744075553208,), fogg=0.0, rain='none', shadow=(StructPoint(dimensions=7, exclude=1, no_shadows=1),), snow=(0.225220769726526,), sun_flare=(StructPoint(exclude=0, no_secondary_flares=5, primary_radius=200, xPos=200, yPos=400),), z_positions=0)
#####################################################
SpacePoint(brightness=(0.17727202600536418,), fogg=0.0, rain='drizzle', shadow=(StructPoint(dimensions=5, exclude=0, no_shadows=2),), snow=(0.8279588032224214,), sun_flare=(StructPoint(exclude=1, no_secondary_flares=15, primary_radius=300, xPos=400, yPos=400),), z_positions=2)
#####################################################
PCA pivot: [0.00144749 0.51333728]
Directions: [[-0.98545982 -0.1699086 ]]
