import numpy as np
import os
from PyQt5.QtCore import QByteArray
from PyQt5.QtGui import QImage
from PyQt5 import QtGui
import veropy
import pickle
import socket

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

# function to run verosim
def execute_single():
    #if not running:
    # remember current work dir
    cwd = os.getcwd()

    # Create a VEROSIM object and pass a config file to it (see config dir for examples)
    print("Creating verosim...")
    vs = veropy.Verosim(CONFIG_VEROSIM)

    # Start VEROSIM only possible once per process
    vs.start(False)

    # change back to the folder of this script
    os.chdir(cwd)

    dt = 100
    dt = int(dt)
    # duration_simulation = 10000
    # duration_simulation = int(duration_simulation)
    # number_of_steps = round(duration_simulation / dt)

    # Create and open a project. Pass project file as uri! Helper will create absolute Url!
    project = vs.open_project(veropy.abs_uri(r"E:\my_model\verosim-models\UrbanTestDrive\UrbanTestDrive_woSUMO-camera.VME"))#E:/my_model/verosim-models/Halifaxstrasse/Halifax_wo_sumo.VME
    # project = vs.open_project(veropy.abs_uri("E:/my_model/verosim-models/verifai_model-Kopie.VME"))
    os.chdir("E:/VEROSIM/006/Target/Bin/Win64VC14_WIDGETS/Release")

    project.start_simulation()
    while True:
        # project.run_simulation_step(dt)
        # vs.process_events()

            # import pickle
            # with open(r"E:\experiment\yolo-camera\yolov5-master\data\verosim_data\file.pkl", 'rb') as file:
            #     pos_x, pos_y, yaw = pickle.load(file)
            ############
            # message = socket.recv()
            # message = dill.loads(b"".join(message))
            # print(message[0], message[1], message[2], '################')
            # socket.send(b"World################")
            ########

        # Create a socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to an address and port
        server_address = ('localhost', 7000)
        sock.bind(server_address)
        # Listen for incoming connections
        sock.listen(1)
        # Accept an incoming connection
        connection, client_address = sock.accept()
        # Receive the data
        data = connection.recv(1024)
        # Convert the received data into a list of integers
        positions = list(data)

        positions[0] += 200 
        positions[1] += 200 
        positions[2] /= 10

        # Print the received list
        print(positions)
        
        # set the car position
        x_pos = "419100"
        # x_pos = "419136"
        instance_port = project.get_instance_by_path(x_pos) 
        property_lv_port = instance_port.property('localValue')   
        scan_val ="double:"+ str(positions[0])
        property_lv_port.set_raw_value(scan_val)
        y_pos = "419101"
        # y_pos = "419137"
        instance_port = project.get_instance_by_path(y_pos) 
        property_lv_port = instance_port.property('localValue')   
        scan_val ="double:" + str(positions[1])
        property_lv_port.set_raw_value(scan_val)
        yaw = "419103"
        # yaw = "419139"
        instance_port = project.get_instance_by_path(yaw) 
        property_lv_port = instance_port.property('localValue')   
        scan_val ="double:" + str(positions[2])
        property_lv_port.set_raw_value(scan_val)
        project.run_simulation_step(dt)
        vs.process_events()

        # read the camera image port
        modelInstanceID = "30"#419120
        # modelInstanceID = "30"   
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        img_raw = property_lv_port.get_raw_value()
        
        # convert the data to qimage
        img_base64 = bytes(img_raw.replace('QImage:',''),'utf-8')
        img_bytes = QByteArray.fromBase64(img_base64)
        img_qimage = QImage()
        img_qimage.loadFromData(img_bytes)

        # convert qimage to numpy
        incomingImage = img_qimage.convertToFormat(QtGui.QImage.Format.Format_RGB32)
        width = incomingImage.width()
        height = incomingImage.height()
        ptr = incomingImage.bits()
        ptr.setsize(height * width * 4)
        image_np = np.frombuffer(ptr, np.uint8).reshape((height, width, 4))
        #cv2.imwrite(r'E:\experiment\a\VerifAI\examples\yolo_camera\image_np.png', image_np)
        ## pickle the image and send to the classifier/client
        # image_np = image_np[:,:,0:3]
        image_np = pickle.dumps(image_np)
        connection.sendall(image_np)
        # Close the connection
        connection.close()
        # Close the socket
        sock.close()

if __name__ == '__main__':
    image_np = execute_single()