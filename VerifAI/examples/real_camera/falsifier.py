import sys
sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
from verifai.features.features import *
from verifai.samplers.feature_sampler import *
from verifai.falsifier import generic_falsifier
from verifai.monitor import specification_monitor
from dotmap import DotMap
import yaml
import numpy as np
import cv2
import os
import glob

# Sampling domain
shadows = Struct({
    # number of shadows to add
    'no_shadows': Categorical(*np.arange(1,3)),#(1,11)
    'dimensions': Categorical(*np.arange(5,8)),#(3,11)
    'exclude': Categorical(*np.arange(0,2))
})

rain = Struct({
    'slant': Categorical(*np.array([-20, -10, 0, 10, 20])),
    'type': Categorical('drizzle', 'heavy', 'torrential'),
    'exclude': Categorical(*np.arange(0,2))
})

sun_flare = Struct({
    # choose flare location relative to the ground truth car location
    'xPos': Categorical(*np.array([-250, -150, 150, 250])),
    'yPos': Categorical(*np.array([-150, -250])),#-250, -150, 
    'no_secondary_flares': Categorical(*np.array([0, 5, 10, 15, 20])),
    'primary_radius': Categorical(*np.array([200, 300])),
    'exclude': Categorical(*np.arange(0,2))
})

                     # set brightness from -1 to 1 instead of darken and brighten
space = FeatureSpace({'brightness': Feature(Box([-0.6, 0.6])),
                      'shadow':Feature(Array(shadows, (1,))),
                      'snow':Feature(Box([0.0, 1.0])),
                      'rain':Feature(Array(rain, (1,))),
                      'sun_flare':Feature(Array(sun_flare, (1,))),
                      'fogg':Feature(Categorical(*np.array([0.0, 0.1, 0.2])))
                      })
#sampler = FeatureSampler.haltonSamplerFor(space)
sampler = FeatureSampler.crossEntropySamplerFor(space)
global IoUs 
global classes 
IoUs = []
classes = []
sampler_folder = 'test'#'haltonSampler'

class confidence_spec(specification_monitor):
    def __init__(self):
        self.m = 0
        # remove the folder contents from the previous run to avoid complications
        files = glob.glob('E:/experiment/my_verifai/VerifAI/examples/yolo_camera/tables/'+sampler_folder+'/*')
        for f in files:
            os.remove(f)

        def specification(traj):
            # check if it is close to the threashold / detection is not a car 
            interesting = bool(traj[0] < 0.8 or traj[1] != 2)
            # interesting = bool(np.abs(0.8 - traj[0]) < 0.1 or (traj[1] != 2 and (traj[1] is not None)))# or traj[0]<0.2 or (traj[0]==0 and not traj[2]))
            if interesting:
                IoUs.append(traj[0])
                # convert the int class to string name
                with open(r"E:\experiment\yolo-camera\yolov5-master\data\coco.yaml", errors='ignore') as stream:  
                    data_loaded = yaml.safe_load(stream)
                str_name = data_loaded['names'][traj[1]]
                classes.append(str_name)
                # save the interesting images
                path = 'E:/experiment/my_verifai/VerifAI/examples/real_camera/tables/'+sampler_folder+'/img'+str(self.m)+'.png'
                cv2.imwrite(path, traj[2])
                self.m += 1
            # check if the IoU is close to 0.7 more than 0.1 points
            #return bool(traj > 0.1)
            # return true if undesired (detection is very good/bad)
            return not interesting
            #return bool(traj['yTrue'] == traj['yPred'])
        super().__init__(specification)


MAX_ITERS = 20##
PORT = 8888
MAXREQS = 5
BUFSIZE = 4096

falsifier_params = DotMap(n_iters=MAX_ITERS,
                          compute_error_table=True,
                          fal_thres=0.5,
                          verbosity=1)

server_options = DotMap(port=PORT, bufsize=BUFSIZE, maxreqs=MAXREQS)

falsifier = generic_falsifier(sampler=sampler, server_options=server_options,
                             monitor=confidence_spec(), falsifier_params=falsifier_params)
falsifier.run_falsifier()

analysis_params = DotMap()
analysis_params.k_closest_params.k = 4
analysis_params.random_params.count = 4
analysis_params.pca = True
analysis_params.k_clusters_params.k = 4
falsifier.analyze_error_table(analysis_params=analysis_params)
#lib = getLib()

print("Error table")
# append IoUs to the table
falsifier.error_table.table.insert(15, "IoU", IoUs)
falsifier.error_table.table.insert(16, "Classes", classes)
print(falsifier.error_table.table)
print("Results of error table analysis")
print("Random samples from error table")
for i, sample in enumerate(falsifier.error_analysis.random_samples):
    print("Random Sample : ", i)
    print(sample)
    # img, _ = genImage(lib, sample)
    # img.save("counterexample_images/random_"+str(i)+".png")
    # img.show()

    # #save new lidar data
    # save_path = "counterexample_lidar"
    # with open(os.path.join(save_path, 'distorted_lidar_'+str(i)+'.bin'), 'wb') as f:
    #     np.save(f, distorted_data)

print("k closest samples from error table")
for i, sample in enumerate(falsifier.error_analysis.k_closest_samples):
    print("Sample : ", i)
    print(sample)
    # img, _ = genImage(lib, sample)
    # img.save("counterexample_images/kclosest_" + str(i) + ".png")

# print("k means clustering centroids from error table")
# print("Centroids for the categorical parts of the sample")
# print(falsifier.error_analysis.k_clusters.keys())

# print("Centroids for the numerical parts of the sample for each discrete cluster")
# for k in falsifier.error_analysis.k_clusters.keys():
#     print(falsifier.error_analysis.k_clusters[k])

print("PCA analysis")
print("PCA pivot: ", falsifier.error_analysis.pca['pivot'])
print("Directions: ", falsifier.error_analysis.pca['directions'])


# To save all samples: uncomment this
# pickle.dump(falsifier.samples, open("generated_samples.pickle", "wb"))

# save the relevant tables to csv
falsifier.error_table.table.to_csv('E:/experiment/my_verifai/VerifAI/examples/real_camera/tables/'+sampler_folder+'/errorTable.csv')
with open('E:/experiment/my_verifai/VerifAI/examples/real_camera/tables/'+sampler_folder+'/kClosest.txt', 'w') as fp:
    for item in falsifier.error_analysis.k_closest_samples:
        fp.write(str(item)+'\n')
        fp.write('#####################################################\n')
    #fp.write(str(falsifier.error_table.table))

    fp.write("PCA pivot: "+ str(falsifier.error_analysis.pca['pivot'])+'\n')
    fp.write("Directions: "+ str(falsifier.error_analysis.pca['directions'])+'\n')
