import numpy as np
from dotmap import DotMap
import sys
sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
from verifai.client import Client

try:
    import tensorflow as tf
except ModuleNotFoundError:
    import sys
    sys.exit('This functionality requires tensorflow to be installed')

import sys
sys.path.append(r'E:\experiment\Automold--Road-Augmentation-Library')
import Automold as am
sys.path.append(r'E:\experiment\yolo-camera\yolov5-master\segment')
from predict_single import main
from test1 import getIoU
import cv2
import os
import pickle
import socket

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

class Classifier(Client):
	def __init__(self, classifier_data):
		port = classifier_data.port
		bufsize = classifier_data.bufsize
		super().__init__(port, bufsize)
		self.sess = tf.compat.v1.Session()
		# start from img index 140
		self.m = 140
		# self.nn = Model()
		# self.nn.init(classifier_data.graph_path, classifier_data.checkpoint_path, self.sess)
		#self.lib = getLib()

	def simulate(self, sample):

		im = cv2.imread('E:/experiment/real_images/frame' + str(self.m) + '.jpg')
		self.m += 1
		# first apply object detection to the original image
		# box, _, _ = main(im, show=False)#
		box, gr_classes, im_ground = main(im, show=False)

		# check if yolo detects any objcets in the original image
		if box is not None:
			# ground = np.array([[int(box[0]), int(box[1])], [int(box[0]), int(box[3])], [int(box[2]), int(box[3])], [int(box[2]), int(box[1])]])
		
			## then apply augmentations
			# rain params
			# ex_rain = sample[2][0][0]
			# rain_slant = sample[2][0][1]
			# rain_type = sample[2][0][2]
			# if ex_rain == 0:
			# 	im = am.add_rain(im, rain_type=rain_type, slant=int(rain_slant))
			
			# # add fog
			# fogg = sample[1]
			# if fogg > 0: im = am.add_fog(im, fogg)
			
			# # sun_flare
			# # add these positions to the ground truth car center
			# ex_flare = sample[5][0][0]
			# x_pos = sample[5][0][3]
			# y_pos = sample[5][0][4]
			# flares = sample[5][0][1]
			# radius = sample[5][0][2]
			# if ex_flare == 0:
			# 	im = am.add_sun_flare(im, flare_center=(int((box[0]+box[2])/2)+x_pos, int((box[1]+box[3])/2)+y_pos), no_of_flare_circles=int(flares), src_radius=int(radius))
			
			# # brightness value
			# brightness = sample[0][0]
			# if brightness > 0.0:
			# 	im = am.brighten(im, brightness)
			# else: im = am.darken(im, -brightness)

			# # snow value
			# # snow = sample[4][0]
			# # im = am.add_snow(im, snow)


			# # car class:2
			# distorted, obj_class, img_out = main(im, show=True)#
			# if distorted is None:
			# 	distorted = np.array([[0, 0], [0, 0], [0, 0], [0, 0]])
			# else:distorted = np.array([[int(distorted[0]), int(distorted[1])], [int(distorted[0]), int(distorted[3])], [int(distorted[2]), int(distorted[3])], [int(distorted[2]), int(distorted[1])]])
			# IoU = getIoU([ground], [distorted])
			# ground = np.array([[int(box[0]), int(box[1])], [int(box[0]), int(box[3])], [int(box[2]), int(box[3])], [int(box[2]), int(box[1])]])
		
			## apply augmentations
			rain_type = sample[2]
			if rain_type != 'none':
				im = am.add_rain(im, rain_type=rain_type)
			
			# add fog
			fogg = sample[1]
			if fogg > 0: im = am.add_fog(im, fogg)
						
			# brightness value
			brightness = sample[0][0]
			if brightness > 0.0:
				im = am.brighten(im, brightness)
			else: im = am.darken(im, -brightness)

			# car class:2
			distorted, obj_class, img_out = main(im, show=False)#
			if distorted is None:
				distorted = [np.array([[0, 0], [0, 0], [0, 0], [0, 0]])]
			#else:distorted = np.array([[int(distorted[0]), int(distorted[1])], [int(distorted[0]), int(distorted[3])], [int(distorted[2]), int(distorted[3])], [int(distorted[2]), int(distorted[1])]])
			IoU = getIoU(box, distorted, img_out.shape)
			print("intersection over uninon", IoU)
		else: 
			print("YOLO could not detect any objects, skipping augmentation.............")
			IoU = 0.0
			obj_class = 80
			img_out = im
			#detected = False
		res = []
		# res.append(np.abs(0.7 - IoU))
		res.append(IoU)			
		if obj_class is None:
			obj_class = 80
		# res.append(obj_class)
		# output of YOLO
		res.append(img_out)
		res.append(im_ground)
		res.append(self.m)
		#return res
		return res



PORT = 8888
BUFSIZE = 4096

classifier_data = DotMap()
classifier_data.port = PORT
classifier_data.bufsize = BUFSIZE
classifier_data.graph_path = './data/car_detector/checkpoint/car-detector-model.meta'
classifier_data.checkpoint_path = './data/car_detector/checkpoint/'

client_task = Classifier(classifier_data)
while True:
	if not client_task.run_client():
		print("End of all classifier calls")
		break
