import numpy as np
import os
# from PyQt5.QtCore import QByteArray
# from PyQt5.QtGui import QImage
# from PyQt5 import QtGui
import veropy
import pickle
import socket
import ast

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

# function to run verosim
def execute_single():
    #if not running:
    # remember current work dir
    cwd = os.getcwd()

    # Create a VEROSIM object and pass a config file to it (see config dir for examples)
    print("Creating verosim...")
    vs = veropy.Verosim(CONFIG_VEROSIM)

    # Start VEROSIM only possible once per process
    vs.start(False)

    # change back to the folder of this script
    os.chdir(cwd)

    dt = 100
    dt = int(dt)
    # duration_simulation = 10000
    # duration_simulation = int(duration_simulation)
    # number_of_steps = round(duration_simulation / dt)

    # Create and open a project. Pass project file as uri! Helper will create absolute Url!
    project = vs.open_project(veropy.abs_uri(r"E:\my_model\verosim-models\UrbanTestDrive\UrbanTestDrive_woSUMO-lidar.VME"))
    # project = vs.open_project(veropy.abs_uri(r"E:\my_model\verosim-models\Halifaxstrasse\Halifax_wo_sumo_lidar.VME"))
    os.chdir("E:/VEROSIM/006/Target/Bin/Win64VC14_WIDGETS/Release")

    project.start_simulation()
    while True:
        # project.run_simulation_step(dt)
        # vs.process_events()

            # import pickle
            # with open(r"E:\experiment\yolo-camera\yolov5-master\data\verosim_data\file.pkl", 'rb') as file:
            #     pos_x, pos_y, yaw = pickle.load(file)
            ############
            # message = socket.recv()
            # message = dill.loads(b"".join(message))
            # print(message[0], message[1], message[2], '################')
            # socket.send(b"World################")
            ########

        # Create a socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to an address and port
        server_address = ('localhost', 7000)
        sock.bind(server_address)
        # Listen for incoming connections
        sock.listen(1)
        # Accept an incoming connection
        connection, client_address = sock.accept()
        # Receive the data
        data = connection.recv(1024)
        # Convert the received data into a list of integers
        positions = list(data)
        # Print the received list
        positions[0] += 200 
        positions[1] += 200 
        positions[2] /= 10
        print(positions)
        # set the car position
        x_pos = "419100"
        instance_port = project.get_instance_by_path(x_pos) 
        property_lv_port = instance_port.property('localValue')
        # case with negative x coord.
        # if positions[2] == 1:
        #     scan_val ="double:"+ str(positions[0])
        #     # scan_val ="double:"+ str(-1*positions[0])
        # else: scan_val ="double:"+ str(positions[0])
        scan_val ="double:"+ str(positions[0])
        property_lv_port.set_raw_value(scan_val)
        y_pos = "419101"
        instance_port = project.get_instance_by_path(y_pos) 
        property_lv_port = instance_port.property('localValue')   
        # scan_val ="double:" + str(positions[1])

        # if positions[2] == 1:
        #     scan_val ="double:"+ str(540 - positions[1])
        # else: scan_val ="double:"+ str(positions[1])
        scan_val ="double:"+ str(positions[1])
        property_lv_port.set_raw_value(scan_val)
        yaw = "419103"
        instance_port = project.get_instance_by_path(yaw) 
        property_lv_port = instance_port.property('localValue')   
        scan_val ="double:" + str(positions[2])
        # scan_val ="double:" + str(positions[3])
        property_lv_port.set_raw_value(scan_val)
        project.run_simulation_step(dt)
        vs.process_events()

        # read lidar values from ports
        # distances
        modelInstanceID = "418659"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        lidar_dist = property_lv_port.get_raw_value()
        lidar_dist = np.array(ast.literal_eval(lidar_dist[16:]))

        # elevation
        modelInstanceID = "418654"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        elevation = property_lv_port.get_raw_value()
        elevation = np.array(ast.literal_eval(elevation[16:]))

        # azimuth
        modelInstanceID = "418653"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        azimuth = property_lv_port.get_raw_value()
        azimuth = np.array(ast.literal_eval(azimuth[16:]))

        # intensity
        modelInstanceID = "418660"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        intensity = property_lv_port.get_raw_value()
        intensity = np.array(ast.literal_eval(intensity[16:]))

        # convert values to KITTI format
        x = lidar_dist*np.cos(elevation)*np.sin(azimuth)
        y = lidar_dist*np.cos(elevation)*np.cos(azimuth)
        z = lidar_dist*np.sin(elevation)
        # reflectivity
        r = intensity/4400
        kitti_lidar = np.c_[x,y,z,r]
        # with open('E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\coords1.bin', 'wb') as f:
        #     np.save(f, kitti_lidar)
        ## pickle the image and send to the classifier/client
        kitti_lidar = pickle.dumps(kitti_lidar)
        connection.sendall(kitti_lidar)
        # Close the connection
        connection.close()
        # Close the socket
        sock.close()

if __name__ == '__main__':
    image_np = execute_single()