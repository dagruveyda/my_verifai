import numpy as np
import os
from PyQt5.QtCore import QByteArray
from PyQt5.QtGui import QImage
from PyQt5 import QtGui
import veropy
import pickle
import socket
import ast

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

# function to run verosim
def execute_single():
    #if not running:
    # remember current work dir
    cwd = os.getcwd()
    os.add_dll_directory("E:/VEROSIM/006/Target/Bin/Win64VC142_WIDGETS/Release")
    os.chdir("E:/VEROSIM/006/Target/Bin/Win64VC142_WIDGETS/Release")
    # Create a VEROSIM object and pass a config file to it (see config dir for examples)
    print("Creating verosim...")
    vs = veropy.Verosim(CONFIG_VEROSIM)

    # Start VEROSIM only possible once per process
    vs.start(False)

    # change back to the folder of this script
    os.chdir(cwd)

    dt = 100
    dt = int(dt)
    # duration_simulation = 10000
    # duration_simulation = int(duration_simulation)
    # number_of_steps = round(duration_simulation / dt)

    # Create and open a project. Pass project file as uri! Helper will create absolute Url!
    #project = vs.open_project(veropy.abs_uri("E:/my_model/verosim-models/verifai_model_lidar.VME"))


    project = vs.open_project(veropy.abs_uri(r"E:\my_model\verosim-models\UrbanTestDrive\UrbanTestDrive_woSUMO.VME"))
    # project = vs.open_project(veropy.abs_uri(r"E:\my_model\verosim-models\Halifaxstrasse\Halifax_wo_sumo_lidar.VME"))

    project.start_simulation()

    while True:

        project.run_simulation_step(dt)
        vs.process_events()
        # read lidar values from ports
        # distances

        modelInstanceID = "418659"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        lidar_dist = property_lv_port.get_raw_value()
        lidar_dist = np.array(ast.literal_eval(lidar_dist[16:]))

        # elevation
        modelInstanceID = "418654"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        elevation = property_lv_port.get_raw_value()
        elevation = np.array(ast.literal_eval(elevation[16:]))

        # azimuth
        modelInstanceID = "418653"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        azimuth = property_lv_port.get_raw_value()
        azimuth = np.array(ast.literal_eval(azimuth[16:]))

        # intensity
        modelInstanceID = "418660"
        instance_port = project.get_instance_by_path(modelInstanceID) 
        property_lv_port = instance_port.property('localValue')   
        intensity = property_lv_port.get_raw_value()
        intensity = np.array(ast.literal_eval(intensity[16:]))

        # convert values to KITTI format
        x = lidar_dist*np.cos(elevation)*np.sin(azimuth)
        y = lidar_dist*np.cos(elevation)*np.cos(azimuth)
        z = lidar_dist*np.sin(elevation)
        # reflectivity
        r = intensity/4400
        kitti_lidar = np.c_[x,y,z,r]
        with open('E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\coords1.bin', 'wb') as f:
            np.save(f, kitti_lidar)
        ## pickle the image and send to the classifier/client
        # kitti_lidar = pickle.dumps(kitti_lidar)
        # connection.sendall(kitti_lidar)
        # # Close the connection
        # connection.close()
        # # Close the socket
        # sock.close()
    	
if __name__ == '__main__':
    image_np = execute_single()