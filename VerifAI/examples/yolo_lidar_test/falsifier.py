# import sys
# sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
# from verifai.features.features import *
# from verifai.samplers.feature_sampler import *
# from verifai.falsifier import generic_falsifier
# from verifai.monitor import specification_monitor
# from dotmap import DotMap
# import yaml
# import numpy as np
# import cv2
# import os
# import glob

# # Sampling domain
# snow = Struct({
#     # number of shadows to add
#     'fall_rate': Categorical(*np.array([0.5, 1.0, 1.5, 2.0, 2.5])),
#     'terminal_velo': Categorical(*np.array([0.2, 0.4, 0.8, 1.0, 1.2, 1.6, 1.8, 2.0])), #for fall_rate=2.0, velo=0.6 no flakes exist
#     'exclude': Categorical(*np.arange(0,2))
# })

# # set the car position
# z_positions = Struct({
#     # 'loc_x': Categorical(*np.array([12, 20, 30, 40, 50])),
#     'loc_x': Categorical(*np.array([10, 25, 40])),
#     # 'loc_x': Categorical(*np.array([-80, -50, -20, 10, 40, 70, 90])),
#     'loc_y': Categorical(*np.array([4, 8, 12])),
#     'minus': Categorical(*np.arange(0,2)), # negative values are not sendable, if this is one, loc_x is negative
#     'yaw': Categorical(*np.arange(0, 6)) #radians
# })

# space = FeatureSpace({'snow':Feature(Array(snow, (1,))),
#                       'fogg':Feature(Box([0.0, 0.3])),
#                       'z_positions': Feature(Array(z_positions, (1,)))
#                       })
# #sampler = FeatureSampler.haltonSamplerFor(space)
# sampler = FeatureSampler.crossEntropySamplerFor(space)
# global IoUs 
# global classes 
# IoUs = []
# sampler_folder = 'crossEntropy'#'haltonSampler'

# class confidence_spec(specification_monitor):
#     def __init__(self):
#         self.m = 0
#         # remove the folder contents from the previous run to avoid complications
#         files = glob.glob('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/*')
#         for f in files:
#             os.remove(f)

#         def specification(traj):
#             # check if it is close to the threashold 
#             interesting = bool(traj[0] < 0.8)
#             if interesting:
#                 IoUs.append(traj[0])

#                 # save the interesting images
#                 cv2.imwrite('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/img'+str(self.m)+'.png', traj[1])
#                 self.m += 1
#             # check if the IoU is close to 0.7 more than 0.1 points
#             #return bool(traj > 0.1)
#             # return true if undesired (detection is very good/bad)
#             return not interesting
#             #return bool(traj['yTrue'] == traj['yPred'])
#         super().__init__(specification)


# MAX_ITERS = 20##
# PORT = 8888
# MAXREQS = 5
# BUFSIZE = 4096

# falsifier_params = DotMap(n_iters=MAX_ITERS,
#                           compute_error_table=True,
#                           fal_thres=0.5,
#                           verbosity=1)

# server_options = DotMap(port=PORT, bufsize=BUFSIZE, maxreqs=MAXREQS)

# falsifier = generic_falsifier(sampler=sampler, server_options=server_options,
#                              monitor=confidence_spec(), falsifier_params=falsifier_params)
# falsifier.run_falsifier()

# analysis_params = DotMap()
# analysis_params.k_closest_params.k = 4
# analysis_params.random_params.count = 4
# analysis_params.pca = True
# analysis_params.k_clusters_params.k = 4
# falsifier.analyze_error_table(analysis_params=analysis_params)
# #lib = getLib()

# print("Error table")
# # append IoUs to the table
# falsifier.error_table.table.insert(9, "IoU", IoUs)
# print(falsifier.error_table.table)
# print("Results of error table analysis")
# print("Random samples from error table")
# for i, sample in enumerate(falsifier.error_analysis.random_samples):
#     print("Random Sample : ", i)
#     print(sample)
#     # img, _ = genImage(lib, sample)
#     # img.save("counterexample_images/random_"+str(i)+".png")
#     # img.show()

#     # #save new lidar data
#     # save_path = "counterexample_lidar"
#     # with open(os.path.join(save_path, 'distorted_lidar_'+str(i)+'.bin'), 'wb') as f:
#     #     np.save(f, distorted_data)

# print("k closest samples from error table")
# for i, sample in enumerate(falsifier.error_analysis.k_closest_samples):
#     print("Sample : ", i)
#     print(sample)
#     # img, _ = genImage(lib, sample)
#     # img.save("counterexample_images/kclosest_" + str(i) + ".png")

# # print("k means clustering centroids from error table")
# # print("Centroids for the categorical parts of the sample")
# # print(falsifier.error_analysis.k_clusters.keys())

# # print("Centroids for the numerical parts of the sample for each discrete cluster")
# # for k in falsifier.error_analysis.k_clusters.keys():
# #     print(falsifier.error_analysis.k_clusters[k])

# print("PCA analysis")
# print("PCA pivot: ", falsifier.error_analysis.pca['pivot'])
# print("Directions: ", falsifier.error_analysis.pca['directions'])


# # To save all samples: uncomment this
# # pickle.dump(falsifier.samples, open("generated_samples.pickle", "wb"))

# # save the relevant tables to csv
# falsifier.error_table.table.to_csv('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/errorTable.csv')
# with open('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/kClosest.txt', 'w') as fp:
#     for item in falsifier.error_analysis.k_closest_samples:
#         fp.write(str(item)+'\n')
#         fp.write('#####################################################\n')
#     fp.write("PCA pivot: "+ str(falsifier.error_analysis.pca['pivot'])+'\n')
#     fp.write("Directions: "+ str(falsifier.error_analysis.pca['directions'])+'\n')

import sys
sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
from verifai.features.features import *
from verifai.samplers.feature_sampler import *
from verifai.falsifier import generic_falsifier
from verifai.monitor import specification_monitor
from dotmap import DotMap
import yaml
import numpy as np
import cv2
import os
import glob

# Sampling domain
snow = Struct({
    # number of shadows to add
    'fall_rate': Categorical(*np.array([0.5, 1.0, 1.5, 2.0, 2.5])),
    'terminal_velo': Categorical(*np.array([0.2, 0.4, 0.8, 1.0, 1.2, 1.6, 1.8, 2.0])), #for fall_rate=2.0, velo=0.6 no flakes exist
    'exclude': Categorical(*np.arange(0,2))
})

# set the car position
# z_positions = Struct({
#     'loc_x': Categorical(*np.array([90, 95, 100])),#290, 295, 300
#     'loc_y': Categorical(*np.array([35, 40, 45, 50])),#235, 240, 245, 250
#     'minus': Categorical(*np.arange(0,2)), # negative values are not sendable, if this is one, loc_x is negative
#     'yaw': Categorical(*np.arange(0, 6)) #radians
# })
# z_positions = Categorical(*np.arange(0,22))

# [[70, 63, -0.31], [77, 60, -0.31], [91, 54, -0.31],
#                                     [77, 60, -0.31], [70, 63, -0.31], [70, 63, -0.31],
#                                     [102, 49, -0.31], [118, 42, -0.31], [116, 47, 2.8],
#                                     [103, 54, 2.8], [84, 59, 2.8], [94, 55, -1.76],
#                                     [92, 39, -1.76], [90, 27, -1.76], [101, 43, 1.85],
#                                     [96, 79, -1.6], [96, 91, -1.6], [96, 98, -1.6],
#                                     [105, 93, 1.54], [105, 79, 1.54], [105, 65, 1.54],
#                                     [98, 25, 1.34]]
space = FeatureSpace({'snow':Feature(Array(snow, (1,))),
                      'fogg':Feature(Box([0.0, 0.3])),
                    #   'z_positions': Feature(z_positions)
                      'z_positions': Feature(Categorical(*np.arange(0,22)))
                      })
#sampler = FeatureSampler.haltonSamplerFor(space)
sampler = FeatureSampler.crossEntropySamplerFor(space)
global IoUs 
global classes 
IoUs = []
sampler_folder = 'crossEntropy'#'haltonSampler'

class confidence_spec(specification_monitor):
    def __init__(self):
        self.m = 0
        # remove the folder contents from the previous run to avoid complications
        files = glob.glob('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/*')
        for f in files:
            os.remove(f)

        def specification(traj):
            # check if it is close to the threashold 
            interesting = bool(traj[0] < 0.8)
            if interesting:
                IoUs.append(traj[0])

                # save the interesting images
                cv2.imwrite('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/img'+str(self.m)+'.png', traj[1])
                cv2.imwrite('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/img_ground'+str(self.m)+'.png', traj[2])
                self.m += 1
            # check if the IoU is close to 0.7 more than 0.1 points
            #return bool(traj > 0.1)
            # return true if undesired (detection is very good/bad)
            return not interesting
            #return bool(traj['yTrue'] == traj['yPred'])
        super().__init__(specification)


MAX_ITERS = 20##
PORT = 8888
MAXREQS = 5
BUFSIZE = 4096

falsifier_params = DotMap(n_iters=MAX_ITERS,
                          compute_error_table=True,
                          fal_thres=0.5,
                          verbosity=1)

server_options = DotMap(port=PORT, bufsize=BUFSIZE, maxreqs=MAXREQS)

falsifier = generic_falsifier(sampler=sampler, server_options=server_options,
                             monitor=confidence_spec(), falsifier_params=falsifier_params)
falsifier.run_falsifier()

analysis_params = DotMap()
analysis_params.k_closest_params.k = 4
analysis_params.random_params.count = 4
analysis_params.pca = True
analysis_params.k_clusters_params.k = 4
falsifier.analyze_error_table(analysis_params=analysis_params)
#lib = getLib()

print("Error table")
# append IoUs to the table
falsifier.error_table.table.insert(5, "IoU", IoUs)
print(falsifier.error_table.table)
print("Results of error table analysis")
print("Random samples from error table")
for i, sample in enumerate(falsifier.error_analysis.random_samples):
    print("Random Sample : ", i)
    print(sample)
    # img, _ = genImage(lib, sample)
    # img.save("counterexample_images/random_"+str(i)+".png")
    # img.show()

    # #save new lidar data
    # save_path = "counterexample_lidar"
    # with open(os.path.join(save_path, 'distorted_lidar_'+str(i)+'.bin'), 'wb') as f:
    #     np.save(f, distorted_data)

print("k closest samples from error table")
for i, sample in enumerate(falsifier.error_analysis.k_closest_samples):
    print("Sample : ", i)
    print(sample)
    # img, _ = genImage(lib, sample)
    # img.save("counterexample_images/kclosest_" + str(i) + ".png")

# print("k means clustering centroids from error table")
# print("Centroids for the categorical parts of the sample")
# print(falsifier.error_analysis.k_clusters.keys())

# print("Centroids for the numerical parts of the sample for each discrete cluster")
# for k in falsifier.error_analysis.k_clusters.keys():
#     print(falsifier.error_analysis.k_clusters[k])

print("PCA analysis")
print("PCA pivot: ", falsifier.error_analysis.pca['pivot'])
print("Directions: ", falsifier.error_analysis.pca['directions'])


# To save all samples: uncomment this
# pickle.dump(falsifier.samples, open("generated_samples.pickle", "wb"))

# save the relevant tables to csv
falsifier.error_table.table.to_csv('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/errorTable.csv')
with open('E:/experiment/my_verifai/VerifAI/examples/yolo_lidar_test/tables/'+sampler_folder+'/kClosest.txt', 'w') as fp:
    for item in falsifier.error_analysis.k_closest_samples:
        fp.write(str(item)+'\n')
        fp.write('#####################################################\n')
    fp.write("PCA pivot: "+ str(falsifier.error_analysis.pca['pivot'])+'\n')
    fp.write("Directions: "+ str(falsifier.error_analysis.pca['directions'])+'\n')