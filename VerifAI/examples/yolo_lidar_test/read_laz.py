import numpy as np
import pylas
import os
# las = pylas.read(r'E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\2021-09-08_11-47-10_5pct_50mm_time_smoothing_world.las')    
# las = pylas.convert(las)  

# with pylas.open(r'E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\2021-09-08_11-47-10_5pct_50mm_time_smoothing_world.laz') as fh:
#     print('Points from Header:', fh.header.point_count)
#     las = fh.read()
#     print(las)
#     print('Points from data:', len(las.points))
#     ground_pts = las.classification == 2
#     bins, counts = np.unique(las.return_number[ground_pts], return_counts=True)
#     print('Ground Point Return Number distribution:')
#     for r,c in zip(bins,counts):
#         print('    {}:{}'.format(r,c))

with open(r'E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\2021-09-08_11-47-10_5pct_50mm_time_smoothing_world.txt', 'r') as txtfile:
    mytextstring = txtfile.read()
from io import StringIO   # StringIO behaves like a file object
str = StringIO(mytextstring)
data = np.loadtxt(str)
data[:,3] /= 4400
# r = data[:,3]/4400
# angles are in radian
# x = data[:,0]*np.cos(data[:,1])*np.sin(data[:,2])
# y = data[:,0]*np.cos(data[:,1])*np.cos(data[:,2])
# z = data[:,0]*np.sin(data[:,1])
with open(r'E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\coords1.bin', 'wb') as f:
    np.save(f, data)
    # np.save(f, np.c_[x,y,z,r])
# b = np.reshape((, 4))
