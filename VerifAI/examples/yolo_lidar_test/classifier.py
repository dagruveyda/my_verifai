import numpy as np
from dotmap import DotMap
import sys
sys.path.append(r'E:\experiment\my_verifai\VerifAI\src')
from verifai.client import Client
try:
    import tensorflow as tf
except ModuleNotFoundError:
    import sys
    sys.exit('This functionality requires tensorflow to be installed')
import os
import pickle
import socket
sys.path.append(r'E:\experiment\yolo-lidar\Complex-YOLOv4-lidar\src')
from test1 import test_single_data
# from calculate_IoU import find_gtruth
# sys.path.append(r'E:\experiment\LISA\python_old')
# from atmos_models import LISA
sys.path.append(r'E:\experiment\LiDAR_snow_sim')
from custom_apply import add_snow, add_fog

CONFIG_VEROSIM = os.path.abspath("D:/KImaDiZ/Ray/configs/release-windows.ini")

class Classifier(Client):
	def __init__(self, classifier_data):
		port = classifier_data.port
		bufsize = classifier_data.bufsize
		super().__init__(port, bufsize)
		self.sess = tf.compat.v1.Session()
		# self.nn = Model()
		# self.nn.init(classifier_data.graph_path, classifier_data.checkpoint_path, self.sess)
		#self.lib = getLib()
		# divide yaw by 10 at the simulator side
		self.pos_array = np.array([[70, 63, 60], [77, 60, 60], [91, 54, 60],
							[77, 60, 60], [70, 63, 60], [70, 63, 60],
							[102, 49, 60], [118, 42, 60], [116, 47, 28],
							[103, 54, 28], [84, 59, 28], [94, 55, 45],
							[92, 39, 45], [90, 27, 45], [101, 43, 18],
							[96, 79, 47], [96, 91, 47], [96, 98, 47],
							[105, 93, 15], [105, 79, 15], [105, 55, 15],
							[98, 25, 13]])

	def simulate(self, sample):
		# Create a socket
		print(sample)

		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect to the server
		server_address = ('localhost', 7000)
		sock.connect(server_address)
		# Send the car position to the server
		pose = self.pos_array[sample[2]]
		# convert to str bec. not possible to send float
		# pose[2] = str(pose[2])
		# numbers = [pose[2][0][0], pose[2][0][1], pose[2][0][2]]
		# numbers = [sample[2][0][0], sample[2][0][1], sample[2][0][2]]
		# numbers = [sample[2][0][0], sample[2][0][1], sample[2][0][2], sample[2][0][3]]
		sock.sendall(bytes(list(pose)))
		# sock.sendall(bytes(numbers))

		# Receive the data
		data = b""
		while True:
			packet = sock.recv(1024)
			if not packet: break
			data += packet
		pc = pickle.loads(data)
		g_truth = [pose[0]+200, pose[1]+200, pose[2]/10] 
		# g_truth = [pose[2][0][0]+200, pose[2][0][1]+200, pose[2][0][2]] 
		# g_truth = [sample[2][0][0]+200, sample[2][0][1]+200, sample[2][0][2]] 
		IoU, img_clean = test_single_data(g_truth, lidarData=pc, display=True)

		# preprocessing step
		MIN_DIST = 3
		pc[:, 3] *= 255
		min_dist_mask = np.linalg.norm(pc[:, 0:3], axis=1) > MIN_DIST
		pc = pc[min_dist_mask, :]
		max_dist_mask = np.linalg.norm(pc[:, 0:3], axis=1) < 120
		pc = pc[max_dist_mask, :]
		min_height_mask = pc[:, 2] > (-400 / 100)  # in m
		pc = pc[min_height_mask, :]
		# apply augmentations
		if sample[1][0][0] == 0:
			snow_fall_rate = sample[1][0][1]
			terminal_velo = sample[1][0][2]
			pc = add_snow(pc, snow_fall_rate, terminal_velo)
		pc, _, _ = add_fog(pc, sample[0][0])
		pc[:, 3] /= 255
		# if sample[2][0][2]==1: g_truth[0] *= -1 
		IoU, img_out = test_single_data(g_truth, lidarData=pc, display=True)
		res = []
		res.append(IoU)
		# output of YOLO
		res.append(img_out)
		res.append(img_clean)
		return res


if __name__ == '__main__':
	PORT = 8888
	# BUFSIZE = 8192
	BUFSIZE = 4096

	classifier_data = DotMap()
	classifier_data.port = PORT
	classifier_data.bufsize = BUFSIZE
	classifier_data.graph_path = './data/car_detector/checkpoint/car-detector-model.meta'
	classifier_data.checkpoint_path = './data/car_detector/checkpoint/'

	client_task = Classifier(classifier_data)
	while True:
		if not client_task.run_client():
			print("End of all classifier calls")
			break

