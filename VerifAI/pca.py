import plotly.express as px
from sklearn.decomposition import PCA
import pandas as pd

# df = px.data.iris()
# features = ["sepal_width", "sepal_length", "petal_width", "petal_length"]

# pca = PCA()
# components = pca.fit_transform(df[features])
# labels = {
#     str(i): f"PC {i+1} ({var:.1f}%)"
#     for i, var in enumerate(pca.explained_variance_ratio_ * 100)
# }

# fig = px.scatter_matrix(
#     components,
#     labels=labels,
#     dimensions=range(4),
#     color=df["species"]
# )
# fig.update_traces(diagonal_visible=False)
# fig.show()

df = pd.read_csv(r'E:\experiment\my_verifai\VerifAI\examples\yolo_lidar_test\tables\crossEntropy\errorTable.csv')
df.drop(['Point.rain', 'Point.shadow[0].dimensions', 'Point.shadow[0].exclude', 'Point.shadow[0].no_shadows', 'Point.snow[0]'], axis=1)
arr = df.to_numpy()[:,1:-2]
# print(arr)
pca = PCA(n_components = 2)
  
components = pca.fit_transform(arr)

fig = px.scatter_matrix(
    components,
    labels=['pc1', 'pc2'],
    dimensions=range(2),
    # color=df["species"]
)
  
# explained_variance = pca.explained_variance_ratio_